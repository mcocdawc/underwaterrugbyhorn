#!/bin/sh
lftp sftp://pi:raspberry@172.24.1.1 -e "
    rm -rf underwaterrugbyhorn
    mkdir underwaterrugbyhorn
    cd underwaterrugbyhorn
    put main/main.py;
		chmod 0777 main.py;
    put main/horn.py;
    put main/horndata.py;
    put main/horngui.kv;
    put main/horngui.py;
    put main/hornio.py;
    put main/horntimers.py;
    mkdir hornwidgets
    cd hornwidgets
    put main/hornwidgets/__init__.py
    put main/hornwidgets/gamewidget.py
    put main/hornwidgets/hornpopup.kv
    put main/hornwidgets/hornpopup.py
    put main/hornwidgets/menuwidget.kv
    put main/hornwidgets/menuwidget.py
    put main/hornwidgets/teamwidget.kv
    put main/hornwidgets/teamwidget.py
    put main/hornwidgets/topareawidget.py
    put main/hornwidgets/topareawidget.kv
    mkdir teamscreens
    cd teamscreens
    put main/hornwidgets/teamscreens/__init__.py
    put main/hornwidgets/teamscreens/addlogentryscreen.kv
    put main/hornwidgets/teamscreens/addlogentryscreen.py
    put main/hornwidgets/teamscreens/intinput.py
    put main/hornwidgets/teamscreens/logscreen.kv
    put main/hornwidgets/teamscreens/logscreen.py
    put main/hornwidgets/teamscreens/menuscreen.kv
    put main/hornwidgets/teamscreens/menuscreen.py
    put main/hornwidgets/teamscreens/messagescreen.kv
    put main/hornwidgets/teamscreens/messagescreen.py
    Bye
"
