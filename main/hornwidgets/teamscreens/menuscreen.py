#!/usr/bin/env python
# -*- coding: utf-8 -*-

''' The kivy-language file
:download:`menuscreen.kv <../../main/hornwidgets/teamscreens/menuscreen.kv>` is
interpreted together with this class.
'''


import os
import kivy.metrics
from kivy.app import App
from kivy.lang import Builder
from kivy.uix.screenmanager import Screen


Builder.load_file(os.path.join(os.path.dirname(os.path.realpath(__file__)),
                               'menuscreen.kv'))


class MenuScreen(Screen):
    ''' This screen displays a menu to manipulate the log
    of one team.'''

    def __init__(self, log_widget, team_data, team_color_text, **kwargs):
        self.backend = App.get_running_app().backend
        self.team_data = team_data
        self.team_color_text = team_color_text
        self.log_widget = log_widget
        self.button_height = kivy.metrics.sp(40)
        super(MenuScreen, self).__init__(**kwargs)
