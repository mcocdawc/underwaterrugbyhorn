#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
from kivy.uix.textinput import TextInput


class IntInput(TextInput):
    ''' Provides a text input filed in which only numbers can be entered.
    '''

    pat = re.compile('[^0-9]')

    def insert_text(self, substring, *args, **kwargs):
        if len(self.text) >= 2:
            s = ''
        else:
            s = re.sub(self.pat, '', substring)
        return super(IntInput, self).insert_text(s, *args, **kwargs)
