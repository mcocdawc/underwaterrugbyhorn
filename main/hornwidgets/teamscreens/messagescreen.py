#!/usr/bin/env python
# -*- coding: utf-8 -*-

''' The kivy-language file
:download:`messagescreen.kv <../../main/hornwidgets/teamscreens/\
messagescreen.kv>` is interpreted together with this class.
'''

import os
from kivy.lang import Builder
from kivy.properties import StringProperty
from kivy.uix.screenmanager import Screen


Builder.load_file(os.path.join(os.path.dirname(os.path.realpath(__file__)),
                               'messagescreen.kv'))


class MessageScreen(Screen):
    ''' This screen displays a message. It is used when th entry condition
    for a log entry screen is not fulfilled. Set the header and message
    property of this class to display a different message.
    '''
    header = StringProperty('asdf')
    message = StringProperty('asdf')

    def __init__(self, **kwargs):
        super(MessageScreen, self).__init__(**kwargs)
