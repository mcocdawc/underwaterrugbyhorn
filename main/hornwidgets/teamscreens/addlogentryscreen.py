#!/usr/bin/env python
# -*- coding: utf-8 -*-

''' The kivy-language file
:download:`addlogentryscreen.kv <../../main/hornwidgets/teamscreens/\
addlogentryscreen.kv>` is interpreted together with this class.
'''

import os
from kivy.app import App
from kivy.lang import Builder
from kivy.logger import Logger
from kivy.uix.label import Label
from kivy.uix.screenmanager import Screen
from kivy.uix.stacklayout import StackLayout


Builder.load_file(os.path.join(os.path.dirname(os.path.realpath(__file__)),
                               'addlogentryscreen.kv'))


class AddLogEntryScreen(Screen):
    ''' This screen displays a menu to manipulate the log
    of one team.'''

    def __init__(self,
                 log_widget,
                 team_data,
                 team_color_text,
                 log_type_str,
                 header_str,
                 event_time='',
                 use_player_number=False,
                 use_reason=False,
                 use_leave_empty_message=False,
                 use_team_checkbox=False,
                 use_doublePentalyTime_checkbox=False,
                 extra_functions=[],
                 entry_condition=False,
                 false_entry_message='',
                 **kwargs):
        '''
        :param string name: Identifer of the screen. Inherited from super.
        :param widget log_widget: Is not used.
        :param main.horndata.TeamData team_data: The team this log will be
            added to.
        :param string team_color_text: The color of the team to dispaly.
        :param string log_type_str: The kind of log event. Text to display.
        :param string header_str: The header of the widget.
        :param int event_time: The length of the log event, eg. the length of
            a penalty.
        :param bool use_player_number: Show the field to enter the player
            number.
        :param bool use_reason: Show the field to enter the reason of the
            log event.
        :param bool use_leave_empty_message: Display the message to leave
            fields empty if unknown.
        :param bool use_team_checkbox: Show the checkbox to apply the log to
            the whole team. Not implemented, jet.
        :param bool use_doublePentalyTime_checkbox: Show the checkbox to
            multiply the *event_time* times 2.
        :param extra_functions: These functions
           will be additionally called, when the log event is created.
        :type extra_functions: list[(callable, list[args])]
        :param callable entry_condition: Is checked before the window opens. If
            FALSE the *false_entry_message* will be displayed instead. Needs to
            retrun TURE or FALSE.
        :param sting false_entry_message: Message to display if
            *entry_condition* returns FALSE.

        .. todo:: Implement team checkbox.
        '''
        self.backend = App.get_running_app().backend
        self.team_data = team_data
        self.team_color_text = team_color_text
        self.log_widget = log_widget
        self.log_type_str = log_type_str
        self.event_time = event_time
        self.use_player_number = use_player_number
        self.use_reason = use_reason
        self.use_leave_empty_message = use_leave_empty_message
        self.use_team_checkbox = use_team_checkbox
        self.use_doublePentalyTime_checkbox = use_doublePentalyTime_checkbox
        self.extra_functions = extra_functions
        if callable(entry_condition):
            self.entry_condition = entry_condition
        else:
            self.entry_condition = self.return_true
        self.false_entry_message = false_entry_message

        super(AddLogEntryScreen, self).__init__(**kwargs)

        self.stack_widget = self.ids['main_stack']

        self.stack_widget.add_widget(HeaderLabel(header_str,
                                                 team_color_text,
                                                 font_size=40))
        if use_player_number:
            self.player_number_widget = PlayerNumber()
            self.stack_widget.add_widget(self.player_number_widget)
        if use_reason:
            self.reason_widget = Reason()
            self.stack_widget.add_widget(self.reason_widget)
        if use_doublePentalyTime_checkbox:
            self.doublePentalyTime_checkbox = DoublePenalty()
            self.stack_widget.add_widget(self.doublePentalyTime_checkbox)
        if use_leave_empty_message:
            self.stack_widget.add_widget(Message(
                'Leer lassen, falls nicht bekannt',
                font_size=25))

    def add_log_event(self):
        if self.entry_condition():
            # check if player number is used
            if self.use_player_number:
                player_number = self.player_number_widget.text_widget.text
                if player_number == '':
                    player_number = 0
                else:
                    try:
                        player_number = int(player_number)
                    except ValueError as e:
                        Logger.error(
                            'Cannot cast palyer number to int:\n' + e)
                        player_number = 0
            else:
                player_number = 0

            # check if reason is used
            if self.use_reason:
                reason = self.reason_widget.text_widget.text
            else:
                reason = ''

            log_type_str = self.log_type_str
            if self.use_doublePentalyTime_checkbox:
                if self.doublePentalyTime_checkbox.checkbox_widget.active:
                    log_type_str = "Doppelte " + self.log_type_str

            # check if event_time is used
            if self.event_time in self.backend.event_times:
                event_time = self.backend.event_times[self.event_time]
                if self.use_doublePentalyTime_checkbox:
                    if self.doublePentalyTime_checkbox.checkbox_widget.active:
                        event_time = 2 * event_time
            else:
                event_time = 0

            # create log event
            log_event = self.backend.add_log_event(
                self.team_data,
                log_type_str=log_type_str,
                player_number=player_number,
                reason=reason,
                event_time=event_time)

            # call some extra functions somewhere else as given in
            # the constructor
            for function, args in self.extra_functions:
                function(*args)

            # go back to log_screen
            self.parent.current = 'logscreen'

            return log_event
        else:
            self.error()
            return None

    def rest(self):
        if self.use_player_number:
            self.player_number_widget.text_widget.text = ''
        if self.use_reason:
            self.reason_widget.text_widget.text = ''
        if self.use_doublePentalyTime_checkbox:
            self.doublePentalyTime_checkbox.checkbox_widget.active = False

    def return_true(self):
        return True

    def error(self):
        message_screen = self.parent.get_screen('messagescreen')
        message_screen.header = "So geht's nicht!"
        message_screen.message = self.false_entry_message
        self.parent.current = 'messagescreen'


class HeaderLabel(Label):
    def __init__(self, header_str, team_color_text, **kwargs):
        self.header_str = header_str
        self.team_color_text = team_color_text
        super(HeaderLabel, self).__init__(**kwargs)


class PlayerNumber(StackLayout):
    def __init__(self):
        super(PlayerNumber, self).__init__()
        self.text_widget = self.ids['text_player_number']


class Reason(StackLayout):
    def __init__(self, **kwargs):
        super(Reason, self).__init__(**kwargs)
        self.text_widget = self.ids['text_reason']


class DoublePenalty(StackLayout):
    def __init__(self):
        super(DoublePenalty, self).__init__()
        self.checkbox_widget = self.ids['checkbox_isDoublePentaly']


class Message(Label):
    def __init__(self, message, **kwargs):
        self.message = message
        super(Message, self).__init__(**kwargs)
