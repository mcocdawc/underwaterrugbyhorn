#!/usr/bin/env python
# -*- coding: utf-8 -*-

''' The kivy-language file
:download:`logscreen.kv <../../main/hornwidgets/teamscreens/logscreen.kv>` is
interpreted together with this class.
'''

import os
import kivy.metrics
from kivy.lang import Builder
from kivy.logger import Logger
from kivy.properties import StringProperty
from kivy.uix.screenmanager import Screen
from kivy.uix.anchorlayout import AnchorLayout


Builder.load_file(os.path.join(os.path.dirname(os.path.realpath(__file__)),
                               'logscreen.kv'))


class LogScreen(Screen):
    ''' This screen displays the log of one team.'''

    def __init__(self, team_data, team_color_text, **kwargs):
        self.team_data = team_data
        self.team_color_text = team_color_text
        self.dropdown_button_height = kivy.metrics.sp(55)
        super(LogScreen, self).__init__(**kwargs)
        self.log_widget = self.ids['log_widget']
        self.team_data.bind(on_reset=self.reset)
        self.team_data.bind(on_new_log_event=self.update_log_events)
        self.update_log_events()

    def add_log_event(self, log_event):
        new_entry = MyListEntry()
        new_entry.id = str(log_event.id)
        new_entry.text = log_event.text
        log_event.bind(text=new_entry.setter('text'))
        self.log_widget.add_widget(new_entry)

    def update_log_events(self, *args, **kwargs):
        Logger.debug("LogScreen: " + self.team_color_text +
                     " update_log_events()")
        self.log_widget.clear_widgets()
        for log_event in self.team_data.log_events:
            self.add_log_event(log_event)

    def reset(self, *args, **kwargs):
        self.update_log_events()


class MyListEntry(AnchorLayout):
    text = StringProperty('')
