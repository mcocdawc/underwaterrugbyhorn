#!/usr/bin/env python
# -*- coding: utf-8 -*-

''' The kivy-language file
:download:`teamwidget.kv <../../main/hornwidgets/teamwidget.kv>` is interpreted
together with this class.
'''

import os
from kivy.app import App
from kivy.lang import Builder
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.screenmanager import ScreenManager
from hornwidgets.teamscreens.logscreen import LogScreen
from hornwidgets.teamscreens.menuscreen import MenuScreen
from hornwidgets.teamscreens.addlogentryscreen import AddLogEntryScreen
from hornwidgets.teamscreens.messagescreen import MessageScreen


Builder.load_file(os.path.join(os.path.dirname(os.path.realpath(__file__)),
                               'teamwidget.kv'))


class TeamWidget(RelativeLayout):
    ''' This widget displays all the information and modification
    possibilies for one team. It consits of all the teamscreens.'''

    def __init__(self, team_color, **kwargs):
        self.backend = App.get_running_app().backend
        self.team_color = team_color
        if team_color == 'blue':
            self.team_color_text = 'Blau'
            self.background = (0.22, 0.67, 0.83, 1)
            self.team_data = self.backend.data.blue
        elif team_color == 'white':
            self.team_color_text = 'Weiß'
            self.background = (0.95, 0.95, 0.95, 1)
            self.team_data = self.backend.data.white
        else:
            msg = ("Team color " + str(team_color) + " is supported. Choos "
                   "either 'blue' or 'white'.")
            raise ValueError(msg)

        super(TeamWidget, self).__init__(**kwargs)

        self.sm = ScreenManager()
        self.add_widget(self.sm)

        self.log_screen = LogScreen(
            name='logscreen',
            team_data=self.team_data,
            team_color_text=self.team_color_text)
        self.sm.add_widget(self.log_screen)

        self.sm.add_widget(MenuScreen(
            log_widget=self.log_screen,
            name='menuscreen',
            team_data=self.team_data,
            team_color_text=self.team_color_text))

        self.sm.add_widget(AddLogEntryScreen(
            name='goalscreen',
            log_widget=self.log_screen,
            team_data=self.team_data,
            team_color_text=self.team_color_text,
            log_type_str='Tor',
            header_str='Tor für ',
            use_player_number=True,
            use_reason=False,
            use_leave_empty_message=True,
            extra_functions=[(self.team_data.add_goal, [])]))

        self.sm.add_widget(AddLogEntryScreen(
            name='warningscreen',
            log_widget=self.log_screen,
            team_data=self.team_data,
            team_color_text=self.team_color_text,
            log_type_str='Verwarnung',
            header_str='Verwarnung gegen ',
            use_player_number=True,
            use_reason=True,
            use_leave_empty_message=False))

        self.sm.add_widget(AddLogEntryScreen(
            name='freethrowscreen',
            log_widget=self.log_screen,
            team_data=self.team_data,
            team_color_text=self.team_color_text,
            log_type_str='Freiwurf',
            header_str='Freiwurf gegen ',
            use_player_number=True,
            use_reason=True,
            use_leave_empty_message=True))

        self.sm.add_widget(AddLogEntryScreen(
            name='penaltythrowscreen',
            log_widget=self.log_screen,
            team_data=self.team_data,
            team_color_text=self.team_color_text,
            log_type_str='Strafwurf',
            header_str='Strafwurf gegen ',
            use_player_number=True,
            use_reason=True,
            use_leave_empty_message=True,
            entry_condition=self.backend.is_penalty_ball_possible,
            false_entry_message=('Das Speil muss angehalten sein, damit ' +
                                 'ein Strafwurf geworfen werden kann.\n\n' +
                                 'Auch können keine Strafwürfe während einer' +
                                 'Halbzeit geworfen werden.'),
            extra_functions=[(self.backend.penalty_ball, [True])]))

        self.sm.add_widget(AddLogEntryScreen(
            name='penaltytimescreen',
            log_widget=self.log_screen,
            team_data=self.team_data,
            team_color_text=self.team_color_text,
            log_type_str='Zeitstrafe',
            header_str='Zeitstrafe gegen ',
            event_time='penaltytime_length',
            use_player_number=True,
            use_reason=True,
            use_leave_empty_message=True,
            use_doublePentalyTime_checkbox=True))

        self.sm.add_widget(AddLogEntryScreen(
            name='expelledscreen',
            log_widget=self.log_screen,
            team_data=self.team_data,
            team_color_text=self.team_color_text,
            log_type_str='Hinausstellung',
            header_str='Hinausstellung gegen ',
            event_time='expelled_length',
            use_player_number=True,
            use_reason=True,
            use_leave_empty_message=True))

        self.sm.add_widget(AddLogEntryScreen(
            name='timeoutscreen',
            log_widget=self.log_screen,
            team_data=self.team_data,
            team_color_text=self.team_color_text,
            log_type_str='Auszeit',
            header_str='Auszeit für ',
            use_player_number=False,
            use_reason=False,
            use_leave_empty_message=False,
            entry_condition=self.is_timeout_possible,
            false_entry_message=('Du kannst nur eine Auszeit pro Team pro ' +
                                 'Halbzeit vergeben.\n\nZudem muss das ' +
                                 'Spiel ' +
                                 'angehalten sein und es muss noch reguläre ' +
                                 'Spielzeit übrig sein.'),
            extra_functions=[(self.team_data.add_timeout, []),
                             (self.backend.start_timeout, [])]))

        self.sm.add_widget(MessageScreen(name='messagescreen'))

    def add_log_event(self, log_event):
        self.log_screen.add_log_event(log_event)

    def is_timeout_possible(self):
        if self.team_data.all_timeouts_taken():
            return False
        if not self.backend.is_stopped():
            return False
        return True
