#!/usr/bin/env python
# -*- coding: utf-8 -*-

''' The kivy-language file
:download:`hornpopup.kv <../../main/hornwidgets/hornpopup.kv>` is interpreted
together with this class.
'''

import os
from kivy.app import App
from kivy.lang import Builder
from kivy.uix.popup import Popup
from kivy.uix.label import Label
from kivy.uix.stacklayout import StackLayout


Builder.load_file(os.path.join(os.path.dirname(os.path.realpath(__file__)),
                               'hornpopup.kv'))


class HornPopup(Popup):
    def __init__(self, title, text, input_fields=[], **kwargs):
        ''' This creates a popup, which is shown in the middle af the
        screen and greys out all the rest.

        :param string title: Title of the popup.
        :param string text: text shown below the title.
        :param List[string] input_fields: List with the labels of input fileds.

        .. warning:: This calss does't work and will cause the program to
            crash.

        .. todo:: Fix hornpopup.
        '''
        super(HornPopup, self).__init__(**kwargs)
        self.title = title
        self.content = HornPopupContent(text, input_fields)
        print self.content.size
        self.size = self.content.size

    def set_size(self, instance, value):
        print 'set_size'
        print value
        print self.size
        self.size = value
        print value
        print self.size

    def save_data(self):
        self.dismiss()

    def cancel(self):
        self.dismiss()


class HornPopupContent(StackLayout):
    def __init__(self, text, input_fields=[], **kwargs):
        super(HornPopupContent, self).__init__(**kwargs)
        self.backend = App.get_running_app().backend
        self.input_fields = input_fields
        self.text_label = PopupLabel()
        self.add_widget(self.text_label)
        print self.size
        height = 0
        width = 0
        for child in self.children:
            print child.height
            print child.width
            height = height + child.height
            width = width + child.width

        self.size = (width, height)
        print 'new'
        print self.size


class PopupLabel(Label):
    pass
