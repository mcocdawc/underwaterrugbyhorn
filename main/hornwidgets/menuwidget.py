#!/usr/bin/env python
# -*- coding: utf-8 -*-

''' The kivy-language file
:download:`menuwidget.kv <../../main/hornwidgets/menuwidget.kv>` is interpreted
together with this class.
'''

import os
from kivy.app import App
from kivy.lang import Builder
from kivy.uix.button import Button
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.stacklayout import StackLayout
from hornwidgets.hornpopup import HornPopup


Builder.load_file(os.path.join(os.path.dirname(os.path.realpath(__file__)),
                               'menuwidget.kv'))


class MenuWidget(RelativeLayout):
    ''' This widget supplies a stack of buttons.
    These are meant for general options.'''
    def __init__(self, **kwargs):
        self.backend = App.get_running_app().backend
        super(MenuWidget, self).__init__(**kwargs)

        self.stack_widget = StackLayout(orientation='lr-tb')
        self.add_widget(self.stack_widget)

        self.reset_button = MenuButton(
            text="Spiel zurück\nsetzen",
            function=self.backend.reset_game,
            func_args={})
        self.cancel_penalty_throw_button = MenuButton(
            text="Strafwurf\nabbrechen",
            function=self.backend.penalty_ball,
            func_args={'turn_on': False})
        self.save_game_button = MenuButton(
            text="Spiel\nspeichern",
            function=self.backend.save_game,
            func_args={})
        self.popup_button = MenuButton(
            text="Zeige\nPopup",
            function=self.create_popup,
            func_args={'title': 'Test Title',
                       'text': 'Test Text',
                       'input_fields': {}})
        self.start_game_button = MenuButton(
            text="Spiel\nstarten",
            function=self.backend.start_game,
            func_args={})
        self.force_start_game_button = MenuButton(
            text="FORCE\nSpiel\nstarten",
            function=self.backend.force_start_game,
            func_args={})
        self.stop_game_button = MenuButton(
            text="Spiel\nstoppen",
            function=self.backend.stop_game,
            func_args={})

        self.stack_widget.add_widget(self.reset_button)
        self.stack_widget.add_widget(self.cancel_penalty_throw_button)
        self.stack_widget.add_widget(self.save_game_button)
        self.stack_widget.add_widget(self.popup_button)
        self.stack_widget.add_widget(self.start_game_button)
        self.stack_widget.add_widget(self.force_start_game_button)
        self.stack_widget.add_widget(self.stop_game_button)
        self.disable_penalty_throw_button()
        self.popup_button.disabled = True
        self.backend.data.bind(
            is_penalty_ball=self.disable_penalty_throw_button)

    def disable_penalty_throw_button(self, *args, **kwargs):
        self.cancel_penalty_throw_button.disabled = (
            not self.backend.data.is_penalty_ball)

    def create_popup(self, **kwargs):
        popup = HornPopup(**kwargs)
        popup.open()


class MenuButton(Button):
    def __init__(self, text, function, func_args, **kwargs):
        self.text = text
        self.function = function
        self.func_args = func_args
        super(MenuButton, self).__init__(**kwargs)
