#!/usr/bin/env python
# -*- coding: utf-8 -*-

''' The kivy-language file
:download:`topareawidget.kv <../../main/hornwidgets/topareawidget.kv>` is
interpreted together with this class.
'''

import os
import RPi.GPIO as GPIO
from kivy.app import App
from kivy.lang import Builder
from kivy.logger import Logger
from kivy.uix.label import Label
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.relativelayout import RelativeLayout


Builder.load_file(os.path.join(os.path.dirname(os.path.realpath(__file__)),
                               'topareawidget.kv'))


class TopAreaWidget(RelativeLayout):
    ''' Widget placed at the top of the screen. It displays the number
    of the halftime, as well as all major timers.'''

    def __init__(self, **kwargs):
        self.backend = App.get_running_app().backend
        self.halftime_area_width = 0.4
        self.time_area_content = GameTimerWidget()
        super(TopAreaWidget, self).__init__(**kwargs)
        self.add_widget(self.time_area_content)

        self.backend.data.bind(is_penalty_ball=self.set_timer_area)
        self.backend.data.bind(is_halftime_break=self.set_timer_area)
        self.backend.data.bind(is_timeout=self.set_timer_area)
        self.backend.data.bind(is_game_over=self.set_timer_area)

    def referee_button_pressed(self):
        ''' This function simulates the pessing of the main referee button,
        if dummy GPIO module is used.'''
        if GPIO.BCM == 'fake':
            GPIO.manual_interrupt(
                self.backend.button_listener.main_referee_pin,
                GPIO.FALLING)

    def referee_button_released(self):
        ''' This function simulates the releasing of the main referee button,
        if dummy GPIO module is used.'''
        if GPIO.BCM == 'fake':
            GPIO.manual_interrupt(
                self.backend.button_listener.main_referee_pin,
                GPIO.RISING)

    def set_timer_area(self, instance, area_name):
        ''' changes the content of the time area. This function is bound to the
        parameter backend.data.is_penalty_ball, backend.data.is_halftime_break,
        backend.data.is_timeout and backend.data.is_game_over and is therefore
        called, whenever these parameter change.'''
        Logger.debug('TopAreaWidget: set_timer_area')
        self.remove_widget(self.time_area_content)
        if self.backend.data.is_timeout:
            self.time_area_content = TimeoutTimerWidget()
        elif self.backend.data.is_penalty_ball:
            self.time_area_content = PenaltyBallTimerWidget()
        elif self.backend.data.is_halftime_break:
            self.time_area_content = HalftimeBreakWidget()
        else:
            self.time_area_content = GameTimerWidget()
        self.add_widget(self.time_area_content)


class MyLabelButton(ButtonBehavior, Label):
    ''' This looks like a label, but you can click on it like a button.'''
    pass


class GameTimerWidget(RelativeLayout):
    ''' Displays the game timer. This widget is meant for the
    time_area_content. '''
    def __init__(self, **kwargs):
        self.backend = App.get_running_app().backend
        self.main_timer_width = 0.5
        self.main_timer_height = 0.7
        super(GameTimerWidget, self).__init__(**kwargs)


class HalftimeBreakWidget(RelativeLayout):
    ''' Displays the halftime berak timer. This widget is meant for the
    time_area_content. '''
    def __init__(self, **kwargs):
        self.backend = App.get_running_app().backend
        self.main_timer_width = 0.5
        self.main_timer_height = 0.7
        super(HalftimeBreakWidget, self).__init__(**kwargs)


class PenaltyBallTimerWidget(RelativeLayout):
    ''' Displays the game timer. This widget is meant for the
    time_area_content. '''
    def __init__(self, **kwargs):
        self.backend = App.get_running_app().backend
        self.main_timer_width = 0.5
        self.main_timer_height = 0.7
        super(PenaltyBallTimerWidget, self).__init__(**kwargs)


class TimeoutTimerWidget(RelativeLayout):
    ''' Displays the game timer. This widget is meant for the
    time_area_content. '''
    def __init__(self, **kwargs):
        self.backend = App.get_running_app().backend
        self.main_timer_width = 0.5
        self.main_timer_height = 0.7
        super(TimeoutTimerWidget, self).__init__(**kwargs)
