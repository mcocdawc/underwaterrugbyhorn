#!/usr/bin/env python
# -*- coding: utf-8 -*-


''' This is a dummy class which simualtes the general purpose input output
(GPIO) pins of a raspberry pi.
'''

BCM = 'fake'

IN = 1
OUT = 0

LOW = 0
HIGH = 1

FALLING = 0
RISING = 1
BOTH = 2

pin_states = {}

callbacks_rising = {}
callbacks_falling = {}
callbacks_both = {}


def setmode(*args):
    pass


def setup(pin, inout):
    ''' Configures the pin.

    :param int pin: GPIO pin numper of the pin to configure.
    :param inout: Defines the pin as an input or output.
    :type inout: GPIO.IN or GPIO.OUT
    '''
    pin_states[pin] = {'inout': inout,
                       'state': LOW}


def output(pin, state):
    ''' Assignes an value to an output. The pin needs to be configued as an
    output before a value can be assigned.

    :param int pin: GPIO pin numper of the pin to assign a value.
    :param state: new output state
    :type state: GPIO.LOW of GPIO.HIGH
    '''
    if pin not in pin_states.keys():
        print 'setup pin before you assign an output value'
    elif pin_states[pin]['inout'] == IN:
        print 'cannot set the value of an input'
    else:
        pin_states[pin]['state'] = state
        print 'Output ' + str(pin) + ': ' + str(state)


def input(pin):
    ''' Retruns the value of an input.

    :param int pin: GPIO pin number to get the imput value from.
    :returns: State of the input pin.
    :rtype: GPIO.LOW or GPIO.HIGH
    '''
    return pin_states[pin]['state']


def add_event_detect(pin, edge, callback):
    ''' Adds a callback to a edge of an input.

    :param int pin: GPIO pin to listen to.
    :param edge: Kind of edge to listen to.
    :type edge: GPIO.RISING, GPIO.FALLING, GPIO.BOTH
    :param callable callback: Function to call when the edge is detected.
    '''
    if pin not in pin_states.keys():
        print 'setup pin before you assign an event'
    elif pin_states[pin]['inout'] == OUT:
        print 'cannot detect event on output'
    elif 'callback' in pin_states[pin].keys():
        print 'alredy set event for this pin'
    else:
        pin_states[pin]['callback'] = {'function': callback,
                                       'edge': edge}


def cleanup():
    ''' Only really neccesary for a real raspbery pi.
    '''
    pass


def manual_interrupt(pin, edge):
    ''' Since this dummy class doesn't listen to real pin, this
    function can simulate an edge at one pin.

    :param int pin: GPIO pin to simulate the edge at. Needs to be an input.
    :param edge: Kind of edge to simulate.
    :type edge: GPIO.RISING or GPIO.FALLING
    '''
    if pin not in pin_states.keys():
        print 'setup pin before you assign an event'
    elif pin_states[pin]['inout'] == OUT:
        print 'cannot interrupt on output'
    elif not (edge == RISING or edge == FALLING):
        print 'edge needs to be rising of falling'
    elif (pin_states[pin]['callback']['edge'] == edge or
          pin_states[pin]['callback']['edge'] == BOTH):
        pin_states[pin]['state'] = edge
        pin_states[pin]['callback']['function'](pin)
