#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
The module hornio handels the in and output via the Raspberry Pi's general
purpose input output pins (GPIO) as well es the detection of sequences of
button actuation.
'''

import threading
import horntimers
import RPi.GPIO as GPIO
from kivy.logger import Logger

DEBUG = False


class ButtonListener (object):
    ''' This class handels the input interrupts and sets the output for the
    horn. It should only be instantiated once, since the pins are hardcoded.'''

    def __init__(self, on_sequence, off_sequence, sequence_detection_time):
        '''
        :param callable on_sequence: Callback function when a sequence is
            deteced on any input.
        :param callable off_sequence: Callback function when a single press is
            detected on main referee input.
        :param int sequence_detection_time: Time span between releasing and
            pressing a button to still count as one sequence in seconds.
        '''
        self.active_buttons = set()
        self.is_blocked = False

        self.lock = threading.Lock()

        self.horn_pin = 4
        self.main_referee_pin = 17
        self.uw_referee_1_pin = 27
        self.uw_referee_2_pin = 22

        GPIO.setmode(GPIO.BCM)   # set up BCM GPIO numbering
        GPIO.setup(self.horn_pin, GPIO.OUT)
        GPIO.output(self.horn_pin, GPIO.LOW)

        # on_sequence = function to call if button is presses sequentially
        # off_sequence = function to call if button is pressed only once or
        #                fisrt press of sequenve
        self.buttons = {
            self.main_referee_pin:
                RefereeButton(self.main_referee_pin,
                              self.button_pressed,
                              self.button_released,
                              sequence_detection_time,
                              on_sequence, off_sequence),
            self.uw_referee_1_pin:
                RefereeButton(self.uw_referee_1_pin,
                              self.button_pressed,
                              self.button_released,
                              sequence_detection_time,
                              on_sequence, None),
            self.uw_referee_2_pin:
                RefereeButton(self.uw_referee_2_pin,
                              self.button_pressed,
                              self.button_released,
                              sequence_detection_time,
                              on_sequence, None)}

    def button_pressed(self, pin):
        ''' Called when a button is pressed.

        :param int pin: Pin number of the pressed input.
        '''
        self.lock.acquire()
        if pin not in self.active_buttons:  # add button to active button list
            self.active_buttons.add(pin)
        if not self.is_blocked:
            GPIO.output(self.horn_pin, GPIO.HIGH)
        self.lock.release()
        # self.buttons[pin].button_pressed()

    def button_released(self, pin):
        ''' Called when a button is released.

        :param int pin: Pin number of the released input.
        '''
        self.lock.acquire()
        if pin in self.active_buttons:  # remove button from active button list
            self.active_buttons.remove(pin)
        if not self.active_buttons:  # stop horn if no more buttons are active
            if not self.is_blocked:
                GPIO.output(self.horn_pin, GPIO.LOW)
        self.lock.release()
        # self.buttons[pin].button_released()

    def sequence(self, sound_list):
        ''' Overrides all buttons and plays sound for the intervals in sound_list

        :param List[int] sound_list: List with times in the format
            [paly_time, break_time, play_time, ...].

        **Example**::

            # 3 honks each 1 second long with .3 second intervals
            sequence([1,.3,1,.3,1])
        '''
        if not self.is_blocked:  # no other sequence is running
            self.is_blocked = True
            if len(sound_list) % 2 == 0:  # make odd -> no break time at end
                sound_list.pop()

            timers = []
            time = 0
            for ii in range(0, len(sound_list) - 1):
                time += sound_list[ii]
                if ii % 2 == 0:  # even: switch horn off
                    args = [self.horn_pin, GPIO.LOW]
                else:  # odd: switch horn on
                    args = [self.horn_pin, GPIO.HIGH]
                timers.append(horntimers.MyTimer(time, GPIO.output, *args))

            timers.append(horntimers.MyTimer(
                time + sound_list[-1], self._sequence_finished))
            for timer in timers:
                timer.start()

            GPIO.output(self.horn_pin, GPIO.HIGH)

    def _sequence_finished(self):
        '''Is called when the sound sequence is over.'''
        self.is_blocked = False
        if not self.active_buttons:
            GPIO.output(self.horn_pin, GPIO.LOW)

    def clean_up(self):
        GPIO.cleanup()


###############################################################################
class RefereeButton(object):
    '''Checks if a button actuation belongs to a actuation sequence ot this
    button.
    '''

    def __init__(self, pin, pressed, released, sequence_detection_time,
                 on_sequence=None, off_sequence=None):
        '''
        :param int pin: GPIO pin number corresponding to this button.
        :param callable pressed: Function to call if button is pressed
        :param callable released: Function to call if button is released.
        :param int sequence_detection_time: Time span between releasing
            and pressing a button to still count as one sequence.
        :param callable on_sequence: Function to call when a sequence is
            deteced this input.
        :param callable off_sequence: Function to all when a single press is
            detected on this input.
        '''
        self.pin = pin
        self.pressed = pressed
        self.released = released
        self.is_on = False
        self.on_sequence = on_sequence
        self.off_sequence = off_sequence
        self.timer = horntimers.MyTimer(
            sequence_detection_time, self._end_sequence)
        self._sequence = False
        self.lock = threading.Lock()
        GPIO.setup(self.pin, GPIO.IN)
        GPIO.add_event_detect(self.pin,
                              GPIO.BOTH,
                              callback=self.button_action)

    def button_action(self, pin):
        ''' Called as soon as any change is detected on the inputs.
        The Button is inverted:

            * relesed button = HIGH on the input after change (0V -> 3.3V)
            * pressed button -> LOW on the input after change (3.3V -> 0V)

        :param int pin: Pin number of the changed input.
        '''
        self.lock.acquire()
        # time.sleep(0.001)
        if DEBUG:
            Logger.debug("        button_action: " +
                         str(pin) + ' (' + str(self.pin) + '): ' +
                         str(GPIO.input(pin)) + ' (' + str(self.is_on) + ')')
        if not GPIO.input(pin) and not self.is_on:
            if DEBUG:
                Logger.debug('        ' + str(pin) + ' pressed')
            self.is_on = True
            self.pressed(pin)
            self.button_pressed()
        elif GPIO.input(pin) and self.is_on:
            if DEBUG:
                Logger.debug('        ' + str(pin) + ' released')
            self.is_on = False
            self.released(pin)
            self.button_released()
        self.lock.release()

    def button_pressed(self, event=None):
        ''' Called when the button is pressed.
        '''
        if self._sequence:
            if self.on_sequence:
                self.on_sequence()
        else:
            if self.off_sequence:
                self.off_sequence()
        self.timer.stop()  # stop timer
        # RefereeButton.lock.release()

    def button_released(self, event=None):
        ''' Called when the button is released.
        '''
        self.timer.restart()  # continue or start a sequence
        self._sequence = True

    def _end_sequence(self):
        ''' Called when internal timer has run out and indicates that
        the sequence is over.'''
        self._sequence = False

    def is_sequence(self):
        ''' Detemines if this button is currently in a sequence.

        :returns: TURE if button is pressed in sequence, else FASLE
        '''
        return self._sequence
