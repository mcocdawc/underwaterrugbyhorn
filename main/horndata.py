#!/usr/bin/python
# -*- coding: utf-8 -*-

import kivy
import numpy as np
import horntimers
from kivy.logger import Logger
from kivy.event import EventDispatcher
from kivy.properties import (StringProperty,
                             BooleanProperty,
                             NumericProperty)

kivy.require('1.9.1')


class GameData(EventDispatcher):
    ''' This class holds all static game data, ie. everything exept the timers.
    '''
    is_penalty_ball = BooleanProperty(False)
    is_halftime_break = BooleanProperty(False)
    is_timeout = BooleanProperty(False)
    is_game_over = BooleanProperty(False)
    n_halftime = NumericProperty(1)

    str_main_timer = StringProperty('')
    str_main_timer_countdown = StringProperty('')
    str_halftime_break_timer = StringProperty('')
    str_penalty_ball_timer = StringProperty('')
    str_timeout_timer = StringProperty('')
    dbl_main_progress = NumericProperty(0.)
    dbl_halftime_break_progress = NumericProperty(0.)
    dbl_penalty_ball_progress = NumericProperty(0.)
    dbl_timeout_progress = NumericProperty(0.)

    def __init__(self, **kwargs):
        ''' :param \*\*kwargs: passed on to super
        '''
        super(GameData, self).__init__(**kwargs)
        self.blue = TeamData('Blau')
        self.white = TeamData('Weiss')

        # these are only initiated here and configured by the
        # configure() function in horn.py
        self.is_continuous_time = False
        self.n_halftime_total = 2

    def reset(self):
        ''' Reset the game data. This will bring the game back into it's
        starting condition.
        '''
        self.blue.reset()
        self.white.reset()
        self.n_halftime = 1
        self.is_penalty_ball = False
        self.is_halftime_break = False
        self.is_timeout = False
        self.is_game_over = False

    def get_all_log_events(self):
        ''' Retruns all log evets from the teamData classes *blue* and *white*.

        :returns: List of log events
        :rtype: List[horndata.LogEvent]
        '''
        return (self.blue.log_events + self.white.log_events)

    def update_display_text(self):
        ''' Updates the text of the log events shown in the gui.
        '''
        self.blue.update_display_text()
        self.white.update_display_text()

    def reset_timeouts(self):
        ''' Resets the number of timeouts per team to 0.
        '''
        self.blue.reset_timeouts()
        self.white.reset_timeouts()

    def get_state(self):
        ''' Creates a dictionary with all important information of this class. This
        dictionary can be used to set this class back into this state.

        :returns: the state of this class
        :rtype: dict
        '''
        state = {"n_halftime": self.n_halftime,
                 "n_halftime_total": self.n_halftime_total,
                 "is_continuous_time": self.is_continuous_time,
                 "is_penalty_ball": self.is_penalty_ball,
                 "is_halftime_break": self.is_halftime_break,
                 "is_timeout": self.is_timeout,
                 "is_game_over": self.is_game_over,
                 "blue": self.blue.get_state(),
                 "white": self.white.get_state()}
        return state

    def set_state(self, state):
        ''' Sets this class' state.

        :param dict state: New state of this class.
        '''
        self.n_halftime = state["n_halftime"]
        self.n_halftime_total = state["n_halftime_total"]
        self.is_continuous_time = state["is_continuous_time"]
        self.is_penalty_ball = state["is_penalty_ball"]
        self.is_halftime_break = state["is_halftime_break"]
        self.is_timeout = state["is_timeout"]
        self.is_game_over = state["is_game_over"]
        self.blue.set_state(state["blue"])
        self.white.set_state(state["white"])


###############################################################################
class TeamData(EventDispatcher):
    ''' This class holds all data specific for one team.
    '''
    goals = NumericProperty(0)

    def __init__(self, team_str, **kwargs):
        '''
        :param string team_str: Name of the team to show in gui
        :param \*\*kwargs: passed on to super
        '''
        self.register_event_type('on_reset')
        self.register_event_type('on_new_log_event')
        super(TeamData, self).__init__(**kwargs)
        self.team_str = team_str
        self.log_events = []
        self.n_timeouts = 0

    def add_log_event(self, **kwargs):
        ''' Adds a new log event to this team. Dispatches the *on_new_log_event*
        event.

        :param \*\*kwargs: passed on to horndata.LogEvent
        '''
        log_event = LogEvent(team_str=self.team_str, **kwargs)
        self.log_events.append(log_event)
        self.dispatch('on_new_log_event')
        return log_event

    def update_display_text(self):
        ''' Updates the text shown on th gui of all log events belonging to this
        team.
        '''
        for log_event in self.log_events:
            log_event.update_display_text()

    def add_goal(self):
        ''' This team scored a goal!
        '''
        self.goals += 1

    def reset(self):
        ''' Resets this team into conditions at the beginning of a new game.
        Dispatches on_reset.
        '''
        self.goals = 0
        for log_event in self.log_events:
            log_event.remove()
        self.log_events = []
        self.n_timeouts = 0
        self.dispatch('on_reset')

    def on_reset(self):
        ''' This is an event. It does nothing by itself, but can notify other classes.
        '''
        pass

    def on_new_log_event(self):
        ''' This is an event. It does nothing by itself, but can notify other classes.
        '''
        pass

    def all_timeouts_taken(self):
        ''' Checks if all timeouts are already taken by this team.

        :returns: TRUE if team has take a timeout already, else FALSE
        '''
        return self.n_timeouts >= 1

    def add_timeout(self):
        ''' This team has taken a timeout.
        '''
        self.n_timeouts += 1

    def reset_timeouts(self):
        ''' Resets the number of timeouts taken y a team. This need to be
        performed after every halftime.
        '''
        self.n_timeouts = 0

    def get_state(self):
        ''' Creates a dictionary with all important information of this class. This
        dictionary can be used to set this class back into this state.

        :returns: the state of this class
        :rtype: dict
        '''
        state = {"team_str": self.team_str,
                 "goals": self.goals,
                 "n_timeouts": self.n_timeouts}
        log_events = []
        for log_event in self.log_events:
            log_events.append(log_event.get_state())
        state["log_events"] = log_events
        return state

    def set_state(self, state):
        ''' Sets this class' state. This function needs to be treated with care,
        since no sanity check is performed before applying the new state.

        :param dict state: New state of this class.
        '''
        self.reset()
        self.team_str = state["team_str"]
        self.goals = state["goals"]
        self.n_timeouts = state["n_timeouts"]
        for log_event_state in state["log_events"]:
            self.log_events.append(create_log_event_from_state(
                log_event_state))
        self.dispatch('on_new_log_event')


###############################################################################
class LogEvent(EventDispatcher):
    ''' This class represents one log event.
    '''
    used_ids = set()
    text = StringProperty('')

    def __init__(self,
                 log_type_str,
                 team_str,
                 n_halftime,
                 game_time,
                 id=0,
                 player_number=0,
                 event_time=0,
                 event_timer=0,
                 reason='',
                 **kwargs):
        '''
        :param (string) log_type_str: Description to show in the gui.
        :param (string)team_str: Team name to sow in the gui.
        :param (int) n_halftime: Number of the halftime this event happend.
        :param (string) game_time: Text with the time of the game when this
            event happend.
        :param (int) id: The unique id of this log event. This is normally set
            automatically and only need to be set here, if an old state of the
            container class is set.
        :param (int) player_number: Number of the palyer, whom this log event
            concers, e.g. th plyer how shot the goal.
        :param (int) event_time=: Lenght if the event timer, e.g. for time
            penalties. The event timer is not created automatically.
        :param (horntimers.MyTimer) event_timer: This is not used and needs to
            be outside of this class.

            .. todo: change the timer are created for logevents.
        :param reason: This reason for a log event, e.g. the reason for a
            penatly.
        :param \*\*kwarg: passed on to super
        '''
        super(LogEvent, self).__init__(**kwargs)
        if id > 0:
            if id in LogEvent.used_ids:
                Logger.debug("LogEvent: the id=" + str(id) +
                             " is already in use")
                return
            else:
                self.id = id
        else:
            if not LogEvent.used_ids:
                self.id = 1
            else:
                self.id = max(LogEvent.used_ids) + 1
        LogEvent.used_ids.add(self.id)
        self.log_type_str = log_type_str
        self.team_str = team_str
        self.n_halftime = n_halftime
        self.game_time = game_time
        game_min, game_sec = np.floor(divmod(self.game_time, 60))
        self.game_time_str = '%02d:%02d' % (game_min, game_sec)
        self.player_number = player_number
        self.event_time = event_time
        self.event_timer = event_timer
        self.reason = reason
        self.update_display_text()

    def update_display_text(self):
        ''' Updates the text shown on th gui of this log event.
        '''
        display = str(self.n_halftime) + '. '
        display += self.game_time_str
        if (not self.event_time == 0 and
                isinstance(self.event_timer, horntimers.MyTimer)):
            min_down, sec_down = self.event_timer.get_countdown_time_ms()
            if not (min_down == 0 and sec_down == 0):
                display += ' (-%02d:%02d)' % (min_down, sec_down)
        display += ' ' + self.log_type_str
        # display += ' ' + self.team
        if not self.player_number == 0:
            display += ' (' + str(self.player_number) + ')'
        if not self.reason == '':
            display += ': ' + self.reason
        if not display == self.text:
            self.text = display

    def remove(self):
        ''' Stop it't timer and delete this log event. '''
        if isinstance(self.event_timer, horntimers.MyTimer):
            self.event_timer.stop()
        LogEvent.used_ids.remove(self.id)

    def get_state(self):
        ''' Creates a dictionary with all important information of this class. This
        dictionary create a new log event exaclt like this one.

        :returns: the state of this class
        :rtype: dict
        '''
        state = {"id": self.id,
                 "log_type_str": self.log_type_str,
                 "team_str": self.team_str,
                 "n_halftime": self.n_halftime,
                 "game_time": self.game_time,
                 "player_number": self.player_number,
                 "event_time": self.event_time,
                 "reason": self.reason}
        if isinstance(self.event_timer, horntimers.MyTimer):
            state["event_timer"] = self.event_timer.get_state()
        return state


def create_log_event_from_state(state):
    ''' Creates a new log event, which has the properties gigen in state.

    :param dict state: State of the new log event.
    :returns: The new log event.
    :rtype: horndata.LogEvent
    '''
    logevent = LogEvent(
        id=state["id"],
        log_type_str=state["log_type_str"],
        team_str=state["team_str"],
        n_halftime=state["n_halftime"],
        game_time=state["game_time"],
        player_number=state["player_number"],
        event_time=state["event_time"],
        reason=state["reason"])
    if "event_timer" in state:
        logevent.event_timer = horntimers.create_my_timer_from_state(
            state["event_timer"])
    return logevent
