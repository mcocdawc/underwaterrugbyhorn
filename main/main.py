#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
This is the main entry into this underwater rugby horn program. All it does is
to check if ``__name__ == "__main__"`` and  start the backend in the main.horn
module as well as the gui in the main.horngui module.
'''

import horn
import horngui
from kivy.logger import Logger, LOG_LEVELS
from kivy.config import Config


Logger.setLevel(level=LOG_LEVELS.get('debug'))
if Config:
    Config.set('modules', 'touchring', 'show_cursor=True,cursor_offset=-24x24')


if __name__ == '__main__':
    backend = horn.Horn()
    with backend:
        horngui.HornGuiApp(backend).run()
