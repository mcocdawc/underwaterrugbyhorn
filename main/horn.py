#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
The module horn provides the backend for the time management.

.. todo::
    check what to do with penalties during continuous time

.. todo::
    check if strafwurf during auszeit is possible,
    show auszeit till finished, then strafwurf

.. todo::
    implement double time penalty
'''

import os
import horntimers
import horndata
import hornio
import json
import socket
from kivy.logger import Logger


class Horn():
    ''' Backend of the underwater rugby horn.

    This class uses new threads to run timers. In order to properly close all
    threads at destruction, it should be used as follows::

        backend = horn.Horn()
        with backend:
            # do something with backend

    This will load the old log from previos instances of this class to
    continue a game after a crash of the raspberry pi when entered. On
    exit it will stop and close all remaining timer threads.
    '''

    def __init__(self):
        Logger.debug('backend: init')

        self.log_filename = "log.json"
        self.log_filename_backup = "log_backup.json"
        self.old_state = {}

        self.broadcast_socket = socket.socket(socket.AF_INET,
                                              socket.SOCK_DGRAM)
        self.broadcast_socket.setsockopt(socket.SOL_SOCKET,
                                         socket.SO_BROADCAST,
                                         1)

        # time between releasing and pressing the button to count
        # as one sequence
        self.sequence_detection_time = 1.5

        # initialize GameData
        self.data = horndata.GameData()
        horntimers.MyCycleTimer(0.5, self.save_state_to_log)

        # this is the standard configuration for liga games
        self.confiugure()

        # initialize the referee buttons
        self.button_listener = hornio.ButtonListener(
            self.stop_game,
            self.start_game,
            self.sequence_detection_time)

    def __enter__(self):
        Logger.debug('backend: enter')
        try:
            self.load_log(self.log_filename)
        except:
            Logger.exception(
                'backend: logfile could not be read, trying backup')
            try:
                self.load_log(self.log_filename_backup)
            except:
                Logger.exception(
                    'backend: also backup logfile could not be read')
                self.reset_game()
                self.confiugure()
        return self

    def __exit__(self, type, value, traceback):
        Logger.info('backend: exit')
        self.button_listener.clean_up()
        # stop all remaining timers, after the main class closed
        for timer_thread in horntimers.MyTimerThread.__all__:
            timer_thread.stopped.set()

    def load_log(self, filename):
        ''' Loads the old log from ``filename`` and stets this class to it's
        state.

        :param string filename: Path to the log to load.
        '''
        
        if os.path.isfile(filename):
            state = {}
            with open(filename, "rb") as f:
                Logger.info('old state: ' + f.read())
                f.seek(0)
                state = json.load(f)
            self.set_state(state)
            if self.data.is_game_over:
                self.reset_game()
            Logger.debug('backend: successfuly loaded old log')

    def confiugure(self,
                   game_length=900,
                   penaltytime_length=120,
                   penalty_ball_length=45,
                   timeout_length=60,
                   expelled_length=300,
                   halftime_break_length=300,
                   n_halftime_total=2,
                   continuous_time=False):
        ''' Sets all the properties of one game. All times are in seconds.

        :param int game_length: Length of one halftime (Halbzeit).
        :param int penaltytime_length: Length of one time penalty (Strafzeit).
        :param int penalty_ball_length: Length of one penalty ball (Strafwurf).
        :param int timeout_length: Lenght of timeout (Auszeit).
        :param int expelled_length: Lenght of short handed time after a player
            was expelled (Hinausstellung).
        :param int halftime_break: Break between halftimes (Halbzeitpause).
            Set to 0 if only one halftime will be played.
        :param bool continuous_time: Don't stop time for any penalties or
            goals.
        '''

        # these are times for the logs.
        self.event_times = {'penaltytime_length': penaltytime_length,
                            'expelled_length': expelled_length}

        self.data.is_continuous_time = continuous_time
        self.data.n_halftime_total = n_halftime_total
        # initialize timers: MyTimer(length of timer, function to call when
        # time is up)
        self.game_timer = horntimers.MyTimer(
            game_length,
            self.game_timer_finished)
        print self.game_timer
        self.penalty_ball_timer = horntimers.MyTimer(
            penalty_ball_length,
            self.penalty_ball_timer_finished)
        self.halftime_break_timer = horntimers.MyTimer(
            halftime_break_length,
            self.halftime_break_over)
        self.timeout_timer = horntimers.MyTimer(
            timeout_length,
            self.timeout_over)

    def game_timer_finished(self):
        ''' This function is called when the game_timer has finished. It
        checks wether a penalty ball is running to prolong the game time and if
        not stops the game.
        '''
        Logger.debug('backend: game_timer_has_ended')
        if self.data.is_penalty_ball:
            Logger.info('Spielzeit wird verlaengert bis Freistoss zu ende ' +
                        'ist.')
        else:
            self.stop_game()

    def penalty_ball_timer_finished(self):
        ''' This function is called, when the penalty_ball_timer has finished.
        It will stop the game.'''
        Logger.debug('backend: penalty_ball_has_ended')
        self.stop_game()

    def timeout_over(self):
        ''' This function is called, when the timeout_timer has finished or
        by force starting the game. A normal start (button press) by the main
        referee will not cause the game to start during timeout.
        '''
        Logger.debug('backend: timeout_timer_has_ended')
        self.timeout_timer.reset()
        self.data.is_timeout = False

    def halftime_break_over(self):
        ''' This function is called when the halftime_break_timer has finished
        or by force starting the game. A normal start (button press) by the
        main referee will not cause the game to start during the
        halftime break.
        '''
        Logger.debug('backend: halftime_break_over')
        self.data.is_halftime_break = False
        self.game_timer.reset()
        self.halftime_break_timer.reset()
        self.penalty_ball_timer.reset()
        self.data.n_halftime += 1

    def halftime_over(self):
        ''' This function is called at the end of each halftime. Ether when
        the game time is up or when the last penalty throw is over. Depending
        on what takes longer.
        '''
        Logger.debug('backend: halftime_over')
        self.button_listener.sequence([1, .3, 1, .3, 1])
        if self.data.n_halftime < self.data.n_halftime_total:
            self.data.is_halftime_break = True
            self.halftime_break_timer.restart()
            self.data.reset_timeouts()
        else:
            self.data.is_game_over = True
        self.add_log_event(team=self.data.blue,
                           log_type_str="- - - Ende der Halbzeit - - -")
        self.add_log_event(team=self.data.white,
                           log_type_str="- - - Ende der Halbzeit - - -")

    def start_game(self):
        ''' This function is called when the main referee presses his
        button once, to start the game or a penalty ball. If he
        presses the button multile times, when the game is stopped, the
        game will be started and immediatly stopped again. During
        timeout or the halftime break the game will not be started.
        Use ``force_start_game`` to start the game during these times.
        '''
        if (self.data.is_halftime_break or self.data.is_timeout):
            Logger.debug('backend: cannot start game, because its halfrtime'
                         ' break or timeout')
        else:
            Logger.debug('backend: start_game')
            self.game_timer.start()
            if self.data.is_penalty_ball:
                self.penalty_ball_timer.start()
            for log_event in self.data.get_all_log_events():
                if isinstance(log_event.event_timer, horntimers.MyTimer):
                    log_event.event_timer.start()

    def force_start_game(self):
        ''' This will end a timeout or a halftime break and start the game.
        '''
        if self.data.is_timeout:
            self.timeout_over()
        if self.data.is_halftime_break:
            self.halftime_break_over()
        self.start_game()

    def stop_game(self):
        ''' This will stop the game, unless if in continuous time mode.
        '''
        Logger.debug('backend: stop_game')
        if not self.data.is_continuous_time:
            self.game_timer.stop()
            for log_event in self.data.get_all_log_events():
                if isinstance(log_event.event_timer, horntimers.MyTimer):
                    log_event.event_timer.stop()
        if self.data.is_penalty_ball:
            # if the game was extended to finish this penalty ball
            if self.game_timer.has_finished() and not self.data.is_game_over:
                self.halftime_over()
            # if the penalty timer has run out, sound a sequence
            elif self.penalty_ball_timer.has_finished():
                self.button_listener.sequence([1, .3, 1, .3, 1])
            self.penalty_ball(False)
        elif not self.data.is_halftime_break:
            if self.game_timer.has_finished():
                if not self.data.is_game_over:
                    self.halftime_over()

    def reset_game(self):
        ''' This function delets all the data of the current game, resets all
        timers, so that a new game can start from teh beginning. This function
        can only be called, if the game is stopped.
        '''
        Logger.debug('backend: reset_game')
        if (not self.is_stopped()):
            Logger.info('Halte das Spiel an bevor du es zurueck setzt.')
            return False
        else:
            self.game_timer.reset()
            self.penalty_ball_timer.reset()
            self.halftime_break_timer.reset()
            self.data.reset()
            return True

    def penalty_ball(self, turn_on):
        ''' Toggles the penatly ball state of this class.

        :param bool turn_on: TRUE to torn on, FALSE to turn off
        '''
        Logger.debug('backend: penatly_ball toggle ' + str(turn_on))
        self.penalty_ball_timer.reset()
        if turn_on:
            if (not self.is_stopped()):
                Logger.info(
                    'Halte das Spiel an bevor du einen Freistoss gibst.')
            else:
                if self.data.is_halftime_break:
                    self.data.is_halftime_break = False
                    self.halftime_break_timer.reset()
                self.data.is_penalty_ball = True
        else:
            self.data.is_penalty_ball = False

    def is_penalty_ball_possible(self):
        ''' Checks wheater a penalty ball is possible at the time.

        :returns: TURE if possible, else FASLE
        '''
        if self.data.is_halftime_break:
            return False
        return self.is_stopped()

    def add_log_event(self, team, event_time=0, **kwargs):
        ''' Creates a log event and adds it to the game data. The time of the
        last break of the game will be used as the time for the log event. In
        continuous time mode it uses the current time for the log envent.

        :param main.horndata.TeamData team: Add the log event to this TeamData
            instance.
        :param int evnet_time: Length of the log event. If not 0, this will add
            a time counter to the log event, e.g. for time penatlies.
        :param \*\*kwargs: These will be forwared to ``team.add_log_event``.
        '''
        Logger.debug('backend: add_log_event')
        if self.data.is_continuous_time:
            game_time = self.game_timer.get_time()
        else:
            game_time = self.game_timer.get_last_break_time()
        log_event = team.add_log_event(game_time=game_time,
                                       n_halftime=self.data.n_halftime,
                                       event_time=event_time,
                                       **kwargs)
        if not event_time == 0:
            t = event_time - (self.game_timer.get_time() - game_time)
            timer = horntimers.MyTimer(t)
            if self.game_timer.is_running():
                timer.start()
            log_event.event_timer = timer
        return log_event

    def is_stopped(self):
        ''' Checks if the game is stopped.

        :returns: FALSE in continuous mode or if game timer is running, else
            TURE.
        '''
        if (not self.data.is_continuous_time and
            (self.game_timer.is_running() or
             self.penalty_ball_timer.is_running())):
            return False
        else:
            return True

    def is_timeout_possible(self):
        ''' Checks if a timeout is possible a this time.

        :returns: TURE if possible, else FALSE
        '''
        if self.game_timer.has_finished():
            return False
        if (self.game_timer.is_running() or
            self.penalty_ball_timer.is_running()):
            return False
        return True

    def short_handed_balance(self, team):
        ''' This function is outdated and needs to be rewritten! It is
        currently not in use.

        If there was a goal while one plyer had a time penalty, this
        function checks weather he is allowed to enter the game again.
        '''
        Logger.debug('backend: short_handed_balance')
        # if (not self.data.is_continuous_time and
        #         (self.game_timer.is_running() or
        #          self.penalty_ball_timer.is_running())):
        # Logger.info('Halte das Spiel an bevor du einen Strafzeit Ausgleich'
        #              'vornimmst.')
        # elif team == 'blue' or team == 'white':
        #     smallest_timer = None
        #     for t, timer in self.penalty_timers:
        #         if t == team:
        #         if not timer.has_finished(): #fist in list unequal to zero
        #            smallest_timer = timer
        #         break
        #     if smallest_timer:
        #         smallest_timer.set_finished()
        # else:
        #     Logger.warning('You can only choose the team colors blue and'
        #                     'white.')

    def start_timeout(self):
        ''' Starts the timeout timer and set the game in timeout state. This is
        only possible if the game is stopped.
        '''
        Logger.debug('backend: start_timeout')
        if not self.is_stopped():
            Logger.info('Du kannst nur eine Auszeit starten, wenn das Spiel'
                        'angehalten ist.')
        else:
            self.game_timer.stop()  # in case of continuous time mode
            self.timeout_timer.restart()
            self.data.is_timeout = True
            self.data.str_time_area_content = 'TimeoutTimer'

    def update_timers(self, *args):
        ''' first updates all strings and other properites
        corresponding to timers in this class and then calls the
        uppdate_penlaty_timers function to update these strings as well.

        :param \*args: not used
        '''
        # halftime break timer
        if self.data.is_halftime_break:
            min_down, sec_down = (self.halftime_break_timer.
                                  get_countdown_time_ms())
            str_down = '-%02d:%02d' % (min_down, sec_down)
            if not str_down == self.data.str_halftime_break_timer:
                self.data.str_halftime_break_timer = str_down
            progress = (self.halftime_break_timer.get_time() /
                        self.halftime_break_timer.time_length)
            if not progress == self.data.dbl_halftime_break_progress:
                self.data.dbl_halftime_break_progress = progress

        # penalty ball timer
        elif self.data.is_penalty_ball:
            min_down, sec_down = (self.penalty_ball_timer.
                                  get_countdown_time_ms())
            str_down = '-%02d:%02d' % (min_down, sec_down)
            if not str_down == self.data.str_penalty_ball_timer:
                self.data.str_penalty_ball_timer = str_down
            progress = (self.penalty_ball_timer.get_time() /
                        self.penalty_ball_timer.time_length)
            if not progress == self.data.dbl_penalty_ball_progress:
                self.data.dbl_penalty_ball_progress = progress

        # timeout timer
        elif self.data.is_timeout:
            min_down, sec_down = self.timeout_timer.get_countdown_time_ms()
            str_down = '-%02d:%02d' % (min_down, sec_down)
            if not str_down == self.data.str_timeout_timer:
                self.data.str_timeout_timer = str_down
            progress = (self.timeout_timer.get_time() /
                        self.timeout_timer.time_length)
            if not progress == self.data.dbl_timeout_progress:
                self.data.dbl_timeout_progress = progress

        # main timer
        else:
            min_up, sec_up = self.game_timer.get_time_ms()
            min_down, sec_down = self.game_timer.get_countdown_time_ms()
            str_up = '%02d:%02d' % (min_up, sec_up)
            str_down = '-%02d:%02d' % (min_down, sec_down)
            if not str_up == self.data.str_main_timer:
                self.data.str_main_timer = str_up
            if not str_down == self.data.str_main_timer_countdown:
                self.data.str_main_timer_countdown = str_down
            progress = self.game_timer.get_time() / self.game_timer.time_length
            if not progress == self.data.dbl_main_progress:
                self.data.dbl_main_progress = progress

        # update display text of all event timers
        self.data.update_display_text()

    def get_state(self):
        ''' Creates a dictionary with all important information of this class. This
        dictionary can be used to set this class back into this state.

        :returns: the state of this class
        :rtype: dict
        '''
        state = {"data": self.data.get_state(),
                 "penaltytime_length": self.event_times['penaltytime_length'],
                 "expelled_length": self.event_times['expelled_length'],
                 "game_timer": self.game_timer.get_state(),
                 "penalty_ball_timer": self.penalty_ball_timer.get_state(),
                 "halftime_break_timer": self.halftime_break_timer.get_state(),
                 "timeout_timer": self.timeout_timer.get_state()}
        return state

    def set_state(self, state):
        ''' Sets this class' state. This function needs to be treated with care,
        since no sanity check is performed before applying the new state.

        :param dict state: New state of this class.
        '''
        Logger.debug('backend: set_state ' + str(state))
        self.data.set_state(state["data"])
        self.event_times['penaltytime_length'] = state["penaltytime_length"]
        self.event_times['expelled_length'] = state["expelled_length"]
        self.game_timer.set_state(state["game_timer"])
        self.penalty_ball_timer.set_state(state["penalty_ball_timer"])
        self.halftime_break_timer.set_state(state["halftime_break_timer"])
        self.timeout_timer.set_state(state["timeout_timer"])

    def save_state_to_log(self):
        ''' Sves the current state of this class to the log file in json format.
        This function is called every 0.5 seconds. 0.25 seconds later it calls
        ``save_state_to_log_backup`` in case the raspberry Pi powered off
        during saving and currupted the logfile.
        '''
        horntimers.MyTimerThread(0.25, self.save_state_to_log_backup).start()
        state = self.get_state()
        data_string = json.dumps(state)
        self.broadcast_socket.sendto(data_string, ('172.24.1.255', 12345))
        if not self.old_state == state:
            with open(self.log_filename, "wb") as f:
                f.write(data_string)
            self.old_state = state

    def save_state_to_log_backup(self):
        ''' Saves the current state of this class to the log backup file in json
        format.
        '''
        state = self.get_state()
        data_string = json.dumps(state)
        self.broadcast_socket.sendto(data_string, ('172.24.1.255', 12345))
        if not self.old_state == state:
            with open(self.log_filename_backup, "wb") as f:
                f.write(data_string)
            self.old_state = state

    def save_game(self):
        ''' Saves the current state of the class to a json file with the
        filename **saved_game_XXX.json**. **XXX** is an increasing number.
        '''
        i = 0
        filename = "saved_game_" + format(i, '03d') + ".json"
        while os.path.isfile(filename):
            i = i + 1
            filename = "saved_game_" + format(i, '03d') + ".json"
        with open(filename, "wb") as f:
            json.dump(self.get_state(), f)
        Logger.info('backend: saved game to file ' + filename)
