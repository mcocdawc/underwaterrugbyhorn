#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Provides all timer classes needed for the UWRhorn module.
'''

import threading
import time
import numpy as np


###############################################################################
class MyTimerThread (threading.Thread):
    ''' Timer which is started in its own thread. Can only be started once
    and dies if it is stopped. All active timers are in the class variable
    *__all__*. Before the program closes all these timer should be stopped,
    so that no running threads are left over.

    .. todo:: connect all timer threads to the main thread, so that thy are
        automatically stopped, when the main thread is stopped. Currently
        they are all stopped maually.
    '''
    __all__ = set()

    def __init__(self, endTime, function, *args, **kwargs):
        '''
        :param int endTime: Time span after which *function* is called
            in seconds.
        :param callable function: This function is called when *endTime* is
            reached.
        '''
        threading.Thread.__init__(self)
        MyTimerThread.__all__.add(self)
        self.endTime = endTime
        self.stopped = threading.Event()
        self.startTime = 0
        self.function = function
        self.args = args
        self.kwargs = kwargs

    def run(self):
        ''' Starts the timer.
        '''
        self.stopped.clear()
        self.startTime = time.time()
        self.stopped.wait(self.endTime)

        if not self.stopped.is_set():
            self.stopped.set()
            self.function(*self.args, **self.kwargs)
        MyTimerThread.__all__.remove(self)

    def getTime(self):
        ''' Gives the time that has passed since the timer was started.

        :returns: passed time
        :rtype: float
        '''
        return time.time() - self.startTime


###############################################################################
class MyTimer():
    ''' Creates a timer, which calls a function when a specified
      time is reached. It can be paused, restarted and reset.'''

    def __init__(self, time_length, function=None, *args, **kwargs):
        '''
        :param float time_length: Time span after which *function* is called in
            seconds.
        :param callable function: This function is called when *time_length* is
            reached.
        '''
        self.time_length = time_length
        self.elapsed_time = 0
        self.timer1 = None
        self.function = function
        self.args = args
        self.kwargs = kwargs

    def finished(self):
        ''' Is called when the internal timer has run out. Calls the function
        specified in the construnctor.
        '''
        self.elapsed_time = self.time_length
        if self.function:
            self.function(*self.args, **self.kwargs)

    def start(self):
        ''' Starts the timer after it has been stopped or reset.
        '''
        if not self.is_running():
            time_diff = self.time_length - self.elapsed_time
            if time_diff > 0:
                self.timer1 = MyTimerThread(time_diff, self.finished)
                self.timer1.start()

    def stop(self):
        ''' Stops the timer, if it is running already.
        '''
        if self.is_running():
            self.elapsed_time = self.elapsed_time + self.timer1.getTime()
            self.timer1.stopped.set()

    def reset(self):
        ''' Stops the timer and resets it to zero.
        '''
        if self.timer1:
            self.timer1.stopped.set()
        self.elapsed_time = 0

    def restart(self):
        ''' Resets the timer and starts it from the beginning.
        '''
        self.reset()
        self.start()

    def is_running(self):
        ''' Checks if the timer is running.

        :returns: TRUE if timer is running, else FALSE
        '''
        if self.timer1 and not self.timer1.stopped.is_set():
            return True
        else:
            return False

    def get_time(self):
        ''' Returns the current time of the timer.

        :returns: Count-up time of the timer.
        :rtype: float
        '''
        if self.is_running():
            return self.elapsed_time + self.timer1.getTime()
        else:
            return self.elapsed_time

    def get_last_break_time(self):
        ''' Retruns the time of the last break of the timer.

        :returns: Time of last break.
        :rtype: float
        '''
        return self.elapsed_time

    def get_countdown_time(self):
        ''' Returns the time left till the timer has finished.

        :returns: Count-down time of timer.
        :rtype: float
        '''
        ct = self.time_length - self.get_time()
        if ct < 0:
            ct = 0
        return ct

    def get_time_ms(self):
        ''' Returns the time in minutes and seconds.

        :returns: Cout-up time of the timer
        :rtype: (int, int)
        '''
        return np.floor(divmod(self.get_time(), 60))

    def get_countdown_time_ms(self):
        ''' Retruns the time left till the timer has finished in minutes and
        seconds.

        :returns: Count-down time of the timer
        :rtype: (int, int)
        '''
        min_down, sec_down = np.ceil(divmod(self.get_countdown_time(), 60))
        if sec_down == 60:
            min_down = min_down + 1
            sec_down = 0
        return min_down, sec_down

    def has_finished(self):
        ''' Checks if the timer has finished.

        :returns: TRUE if time is reached, else FALSE
        '''
        if self.elapsed_time == self.time_length:
            return True
        else:
            return False

    def set_finished(self):
        ''' Sets the timer to it's finished state.'''
        if self.is_running():
            self.stop()
        self.elapsed_time = self.time_length

    def get_state(self):
        ''' Creates a dictionary with all important information of this class. This
        dictionary can be used to set this class back into this state.

        :returns: the state of this class
        :rtype: dict
        '''
        state = {"time_length": self.time_length,
                 "elapsed_time": self.get_time(),
                 "is_running": self.is_running()}
        return state

    def set_state(self, state):
        ''' Sets this class' state. This function needs to be treated with care,
        since no sanity check is performed before applying the new state.

        :param dict state: New state of this class.
        '''
        self.stop()
        self.time_length = state["time_length"]
        self.elapsed_time = state["elapsed_time"]
        if state["is_running"]:
            self.start()


def create_my_timer_from_state(state):
    ''' Creates a timer from a given state.

    :param dict state: State of the new timer.
    :returns: New timer.
    :rtype: main.horntimers.MyTimer
    '''
    timer = MyTimer(time_length=state["time_length"])
    timer.stop()
    timer.elapsed_time = state["elapsed_time"]
    if state["is_running"]:
        timer.start()
    return timer


###############################################################################
class MyCycleTimer():
    ''' Cretes a timer that calles a function periodically.
    '''
    
    def __init__(self, cycle_time, function):
        '''
        :param float cycle_time: Timer interval in which *function* is called.
        :param callable function: Function to call after each interval.
        '''
        self.cycle_time = cycle_time
        self.function = function
        self.timer_thread = MyTimerThread(self.cycle_time, self.cycle)
        self.timer_thread.start()

    def cycle(self):
        ''' Internal function that is called periodically.
        '''
        self.timer_thread = MyTimerThread(self.cycle_time, self.cycle)
        self.timer_thread.start()
        self.function()
