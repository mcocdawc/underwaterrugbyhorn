#!/usr/bin/python
# -*- coding: utf-8 -*-

import kivy
from kivy.properties import (StringProperty,
                             BooleanProperty,
                             NumericProperty)
from kivy.event import EventDispatcher

kivy.require('1.9.1')


class GameData(EventDispatcher):
    ''' This class holds all static game data, ie. everything exept the timers.
    '''
    is_penalty_ball = BooleanProperty(False)
    is_halftime_break = BooleanProperty(False)
    is_timeout = BooleanProperty(False)
    is_game_over = BooleanProperty(False)
    n_halftime = NumericProperty(1)

    str_main_timer = StringProperty('')
    str_main_timer_countdown = StringProperty('')
    str_halftime_break_timer = StringProperty('')
    str_penalty_ball_timer = StringProperty('')
    str_timeout_timer = StringProperty('')
    dbl_main_progress = NumericProperty(0.)
    dbl_halftime_break_progress = NumericProperty(0.)
    dbl_penalty_ball_progress = NumericProperty(0.)
    dbl_timeout_progress = NumericProperty(0.)

    def __init__(self, **kwargs):
        ''' :param \*\*kwargs: passed on to super
        '''
        super(GameData, self).__init__(**kwargs)
        self.blue = TeamData('Blau')
        self.white = TeamData('Weiss')

        # these are only initiated here and configured by the
        # configure() function in horn.py
        self.is_continuous_time = False
        self.n_halftime_total = 2

    def set_state(self, state):
        ''' Sets this class' state.

        :param dict state: New state of this class.
        '''
        if not self.n_halftime == state["n_halftime"]:
            self.n_halftime = state["n_halftime"]
        if not self.n_halftime_total == state["n_halftime_total"]:
            self.n_halftime_total = state["n_halftime_total"]
        if not self.is_continuous_time == state["is_continuous_time"]:
            self.is_continuous_time = state["is_continuous_time"]
        if not self.is_penalty_ball == state["is_penalty_ball"]:
            self.is_penalty_ball = state["is_penalty_ball"]
        if not self.is_halftime_break == state["is_halftime_break"]:
            self.is_halftime_break = state["is_halftime_break"]
        if not self.is_timeout == state["is_timeout"]:
            self.is_timeout = state["is_timeout"]
        if not self.is_game_over == state["is_game_over"]:
            self.is_game_over = state["is_game_over"]
        self.blue.set_state(state["blue"])
        self.white.set_state(state["white"])


###############################################################################
class TeamData(EventDispatcher):
    ''' This class holds all data specific for one team.
    '''
    goals = NumericProperty(0)

    def __init__(self, team_str, **kwargs):
        '''
        :param string team_str: Name of the team to show in gui
        :param \*\*kwargs: passed on to super
        '''
        super(TeamData, self).__init__(**kwargs)
        self.team_str = team_str
        self.n_timeouts = 0

    def set_state(self, state):
        ''' Sets this class' state.

        :param dict state: New state of this class.
        '''
        if not self.team_str == state["team_str"]:
            self.team_str = state["team_str"]
        if not self.goals == state["goals"]:
            self.goals = state["goals"]
        if not self.n_timeouts == state["n_timeouts"]:
            self.n_timeouts = state["n_timeouts"]
