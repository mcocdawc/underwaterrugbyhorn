#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
The module horn provides the backend for the client.
'''

import horntimers
import horndata
import json
import socket
import threading
from kivy.logger import Logger


class Horn():
    ''' Backend of the underwater rugby horn client.

    This class uses new threads to run timers. In order to properly close all
    threads at destruction, it should be used as follows::

        backend = horn.Horn()
        with backend:
            # do something with backend
    '''
    def __init__(self):
        Logger.debug('backend: init')

        # initialize GameData
        self.data = horndata.GameData()

        # this is the standard configuration for liga games
        self.confiugure()

    def __enter__(self):
        Logger.debug('backend: enter')
        self.receiver_thread = ReceiverThread(self.set_state,
                                              self.update_timers)
        self.receiver_thread.start()
        return self

    def __exit__(self, type, value, traceback):
        Logger.info('backend: exit')
        # stop all remaining timers, after the main class closed
        for timer_thread in horntimers.MyTimerThread.__all__:
            timer_thread.stopped.set()
        for receiver_thread in ReceiverThread.__all__:
            receiver_thread.stopped.set()

    def confiugure(self,
                   game_length=900,
                   penaltytime_length=120,
                   penalty_ball_length=45,
                   timeout_length=60,
                   expelled_length=300,
                   halftime_break_length=300,
                   n_halftime_total=2,
                   continuous_time=False):
        ''' Sets all the properties of one game. All times are in seconds.

        :param int game_length: Length of one halftime (Halbzeit).
        :param int penaltytime_length: Length of one time penalty (Strafzeit).
        :param int penalty_ball_length: Length of one penalty ball (Strafwurf).
        :param int timeout_length: Lenght of timeout (Auszeit).
        :param int expelled_length: Lenght of short handed time after a player
            was expelled (Hinausstellung).
        :param int halftime_break: Break between halftimes (Halbzeitpause).
            Set to 0 if only one halftime will be played.
        :param bool continuous_time: Don't stop time for any penalties or
            goals.
        '''

        self.data.is_continuous_time = continuous_time
        self.data.n_halftime_total = n_halftime_total
        # initialize timers: MyTimer(length of timer, function to call when
        # time is up)
        self.game_timer = horntimers.MyTimer(
            game_length)
        self.penalty_ball_timer = horntimers.MyTimer(
            penalty_ball_length)
        self.halftime_break_timer = horntimers.MyTimer(
            halftime_break_length)
        self.timeout_timer = horntimers.MyTimer(
            timeout_length)

    def update_timers(self, *args):
        ''' first updates all strings and other properites
        corresponding to timers in this class and then calls the
        uppdate_penlaty_timers function to update these strings as well.
        This function is also used to save the curreten state of the
        gmae in the log file.'''
        # save log
        # self.save_state_to_log()
        # halftime break timer
        if self.data.is_halftime_break:
            min_down, sec_down = self.halftime_break_timer.get_countdown_time_ms()
            str_down = '-%02d:%02d' % (min_down, sec_down)
            if not str_down == self.data.str_halftime_break_timer:
                self.data.str_halftime_break_timer = str_down
            progress = (self.halftime_break_timer.get_time() /
                        self.halftime_break_timer.time_length)
            if not progress == self.data.dbl_halftime_break_progress:
                self.data.dbl_halftime_break_progress = progress

        # penalty ball timer
        elif self.data.is_penalty_ball:
            min_down, sec_down = self.penalty_ball_timer.get_countdown_time_ms()
            str_down = '-%02d:%02d' % (min_down, sec_down)
            if not str_down == self.data.str_penalty_ball_timer:
                self.data.str_penalty_ball_timer = str_down
            progress = (self.penalty_ball_timer.get_time() /
                        self.penalty_ball_timer.time_length)
            if not progress == self.data.dbl_penalty_ball_progress:
                self.data.dbl_penalty_ball_progress = progress

        # timeout timer
        elif self.data.is_timeout:
            min_down, sec_down = self.timeout_timer.get_countdown_time_ms()
            str_down = '-%02d:%02d' % (min_down, sec_down)
            if not str_down == self.data.str_timeout_timer:
                self.data.str_timeout_timer = str_down
            progress = (self.timeout_timer.get_time() /
                        self.timeout_timer.time_length)
            if not progress == self.data.dbl_timeout_progress:
                self.data.dbl_timeout_progress = progress

        # main timer
        else:
            min_up,   sec_up = self.game_timer.get_time_ms()
            min_down, sec_down = self.game_timer.get_countdown_time_ms()
            str_up = '%02d:%02d' % (min_up, sec_up)
            str_down = '-%02d:%02d' % (min_down, sec_down)
            if not str_up == self.data.str_main_timer:
                self.data.str_main_timer = str_up
            if not str_down == self.data.str_main_timer_countdown:
                self.data.str_main_timer_countdown = str_down
            progress = self.game_timer.get_time() / self.game_timer.time_length
            if not progress == self.data.dbl_main_progress:
                self.data.dbl_main_progress = progress

    def set_state(self, state):
        self.data.set_state(state["data"])
        self.game_timer.set_state(state["game_timer"])
        self.penalty_ball_timer.set_state(state["penalty_ball_timer"])
        self.halftime_break_timer.set_state(state["halftime_break_timer"])
        self.timeout_timer.set_state(state["timeout_timer"])


########################################################################
class ReceiverThread (threading.Thread):
    ''' This creates a thread that is listening on ip 172.24.1.255
    port 12345 and updates the state of the backend.
    '''
    __all__ = set()

    def __init__(self, set_state, update_timers, **kwargs):
        '''
        :param callable set_state: Function which sets the state of the
            backend.
        :param callalble update_timers: Function which updates the display
            texts of the timers.
        :param \*\*kwargs: Not used.
        '''
        threading.Thread.__init__(self)
        ReceiverThread.__all__.add(self)
        self.stopped = threading.Event()
        self.set_state = set_state
        self.update_timers = update_timers
        self.broadcast_socket = socket.socket(socket.AF_INET,
                                              socket.SOCK_DGRAM)
        self.broadcast_socket.settimeout(2)
        try:
            self.broadcast_socket.bind(('172.24.1.255', 12345))
        except Exception as e:
            Logger.exception('ReceiverThread: Could not bind to socket: ' +
                             str(e))
            # raise e
        self.last_data_string = ""

    def run(self):
        self.stopped.clear()

        while not self.stopped.is_set():
            try:
                message = self.broadcast_socket.recv(32768)
            except socket.timeout:
                Logger.exception('ReceiverThread: No new message received ' +
                                 'in the last 2 seconds.')
            else:
                if not message == self.last_data_string:
                    try:
                        self.set_state(json.loads(message))
                        self.last_data_string = message
                    except:
                        Logger.exception('Unknown Message: ' + message)
            self.update_timers()

        ReceiverThread.__all__.remove(self)
