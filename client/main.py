#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
This is the main entry into the client of  underwater rugby horn program. All
it does is to check if ``__name__ == "__main__"`` and  start the backend in
the main.horn module as well as the gui in the main.horngui module.

The client only displays the infromation it receives from the IP 172.24.1.255
on port 12345.
'''

import horngui
import horn
from kivy.logger import Logger, LOG_LEVELS


Logger.setLevel(level=LOG_LEVELS.get('debug'))


if __name__ == '__main__':
    backend = horn.Horn()
    with backend:
        horngui.HornGuiApp(backend).run()
