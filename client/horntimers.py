#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Provides all timer classes needed for the program.
'''

import threading
import time
import numpy as np


########################################################################
class MyTimerThread (threading.Thread):
    ''' Timer which is started in its own thread. Can only be started once
      and dies if it is stopped. '''
    __all__ = set()

    def __init__(self, endTime, function, *args, **kwargs):
        ''' Constructor.
        @param self      The object pointer.
        @param endTime   Time span after which \a function is called
        in seconds.
        @param function  This function is called when \a endTime is
        reached.'''
        threading.Thread.__init__(self)
        MyTimerThread.__all__.add(self)
        self.endTime = endTime
        self.stopped = threading.Event()
        self.startTime = 0
        self.function = function
        self.args = args
        self.kwargs = kwargs

    def run(self):
        self.stopped.clear()
        self.startTime = time.time()
        self.stopped.wait(self.endTime)

        if not self.stopped.is_set():
            self.stopped.set()
            self.function(*self.args, **self.kwargs)
        MyTimerThread.__all__.remove(self)

    def getTime(self):
        return time.time() - self.startTime


########################################################################
class MyTimer():
    ''' Creates a timer, which calls a function when a specified
      time is reached. It can be paused, restarted and reset.'''

    def __init__(self, time_length, function=None, *args, **kwargs):
        ''' Constructor.
        @param self          The object pointer.
        @param time_length   Time span after which \a function is called in
        seconds.
        @param function      This function is called when \a time_length is
        reached.'''
        self.time_length = time_length
        self.elapsed_time = 0
        self.timer1 = None
        self.function = function
        self.args = args
        self.kwargs = kwargs

    def finished(self):
        ''' Is called when the internal timer has run out. Calls the function
        specified in the construnctor.
        @param self  The object pointer.'''
        self.elapsed_time = self.time_length
        if self.function:
            self.function(*self.args, **self.kwargs)

    def start(self):
        ''' Starts the timer after it has been stopped or reset.
        @param self  The object pointer.'''
        pass

    def stop(self):
        ''' Stops the timer, if it is running already.
        @param self  The object pointer.'''
        pass

    def reset(self):
        ''' Stops the timer and resets it to zero.
        @param self  The object pointer.'''
        pass

    def restart(self):
        ''' Resets the timer and starts it from the beginning.
        @param self  The object pointer.'''
        pass

    def is_running(self):
        ''' Checks if the timer is running.
        @param self  The object pointer.
        @return TRUE: timer is running. \n
                FALSE: timer has been stopped or reset.'''
        if self.timer1 and not self.timer1.stopped.is_set():
            return True
        else:
            return False

    def get_time(self):
        ''' Returns the current time of the timer.
        @param self  The object pointer.
        @return Count-up time of the timer.'''
        return self.elapsed_time

    def get_last_break_time(self):
        ''' Retruns the time of the last break of the timer.
        @param self The object pointer.
        @return Time of last break.'''
        pass

    def get_countdown_time(self):
        ''' Returns the time left till the timer has finished.
        @param self  The object pointer.
        @return Count-down time of timer.'''
        ct = self.time_length - self.get_time()
        if ct < 0:
            ct = 0
        return ct

    def get_time_ms(self):
        return np.floor(divmod(self.get_time(), 60))

    def get_countdown_time_ms(self):
        min_down, sec_down = np.ceil(divmod(self.get_countdown_time(), 60))
        if sec_down == 60:
            min_down = min_down + 1
            sec_down = 0
        return min_down, sec_down

    def has_finished(self):
        ''' Retruns TRUE if timer has finished till the set time_length.'''
        if self.elapsed_time == self.time_length:
            return True
        else:
            return False

    def set_finished(self):
        ''' Sets the timer to it's finished state.'''
        if self.is_running():
            self.stop()
        self.elapsed_time = self.time_length

    def get_state(self):
        state = {"time_length": self.time_length,
                 "elapsed_time": self.get_time(),
                 "is_running": self.is_running()}
        return state

    def set_state(self, state):
        if not self.time_length == state["time_length"]:
            self.time_length = state["time_length"]
        if not self.elapsed_time == state["elapsed_time"]:
            self.elapsed_time = state["elapsed_time"]
        if not self.is_running == state["is_running"]:
            self.is_running = state["is_running"]


def create_my_timer_from_state(state):
    timer = MyTimer(time_length=state["time_length"])
    timer.stop()
    timer.elapsed_time = state["elapsed_time"]
    if state["is_running"]:
        timer.start()
    return timer


###########################################################
class MyCycleTimer():

    def __init__(self, cycle_time, function):
        self.cycle_time = cycle_time
        self.function = function
        self.timer_thread = MyTimerThread(self.cycle_time, self.cycle)
        self.timer_thread.start()

    def cycle(self):
        self.timer_thread = MyTimerThread(self.cycle_time, self.cycle)
        self.timer_thread.start()
        self.function()
