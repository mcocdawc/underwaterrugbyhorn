#!/usr/bin/env python
# -*- coding: utf-8 -*-

''' The kivy-language file
:download:`topareawidget.kv <../../client/hornwidgets/teamwidget.kv>` is
interpreted together with this class.
'''


import os
from kivy.app import App
from kivy.lang import Builder
from kivy.uix.relativelayout import RelativeLayout
from kivy.logger import Logger


Builder.load_file(os.path.join(os.path.dirname(os.path.realpath(__file__)),
                               'teamwidget.kv'))


class TeamWidget(RelativeLayout):
    ''' This widget contails all the information and modification
    possibilies for one team.'''

    def __init__(self, team_color, **kwargs):
        Logger.debug('TeamWidget: Init')
        self.backend = App.get_running_app().backend
        self.team_color = team_color
        if team_color == 'blue':
            self.team_color_text = 'Blau'
            self.background = (0.22, 0.67, 0.83, 1)
            self.team_data = self.backend.data.blue
        elif team_color == 'white':
            self.team_color_text = 'Weiß'
            self.background = (0.95, 0.95, 0.95, 1)
            self.team_data = self.backend.data.white
        else:
            msg = ("Team color " + str(team_color) + " is supported. Choos "
                   "either 'blue' or 'white'.")
            raise ValueError(msg)

        super(TeamWidget, self).__init__(**kwargs)
