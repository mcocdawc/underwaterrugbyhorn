#!/usr/bin/env python
# -*- coding: utf-8 -*-

from kivy.app import App
from kivy.uix.relativelayout import RelativeLayout
from hornwidgets.topareawidget import TopAreaWidget
from hornwidgets.teamwidget import TeamWidget
from kivy.logger import Logger


class GameWidget(RelativeLayout):
    ''' This widget contains all the  game information and options. '''

    def __init__(self, **kwargs):
        Logger.debug('GameWidget: Init')
        self.backend = App.get_running_app().backend
        self.top_area_height = .4
        super(GameWidget, self).__init__(**kwargs)
        self.add_widget(TopAreaWidget(
            size_hint=(1, self.top_area_height),
            pos_hint={'x': 0, 'y': (1 - self.top_area_height)}))
        self.add_widget(TeamWidget(
            team_color='blue',
            size_hint=(.5, 1 - self.top_area_height),
            pos_hint={'x': .5, 'y': 0}))
        self.add_widget(TeamWidget(
            team_color='white',
            size_hint=(.5, 1 - self.top_area_height),
            pos_hint={'x': 0, 'y': 0}))
