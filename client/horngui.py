#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
The module horngui provides the entry point horngui.HornGuiApp for the gui.
'''

import kivy
import kivy.metrics
from kivy.app import App
from kivy.uix.floatlayout import FloatLayout


kivy.require('1.9.1')


class HornGuiApp(App):
    ''' This is the entry point class for the gui. To run the gui call::

        horngui.HornGuiApp(backend).run()

    The kivy-language file :download:`horngui.kv <../../client/horngui.kv>` is
    interpted together with this class.
    '''

    def __init__(self, backend, **kwargs):
        '''
        :param client.horn.Horn backend: backend of the gui, which holds all
            the data and manages the time and the sound.
        :param \*\*kwargs: passed on to super
        '''
        self.backend = backend
        super(HornGuiApp, self).__init__(**kwargs)

    def build(self):
        ''' This function is needed by super.run(). It needs to retrun the main
        widget.

        :retruns: the main widget
        '''
        return MainWidget()


class MainWidget(FloatLayout):
    ''' This is the main widget. Everything needs to be displayed within this
    widget. All sub-widgets are located in the package *hornwidgets*.'''
    def __init__(self, **kwargs):
        ''' :param \*\*kwargs: Passed on to super.
        '''
        self.backend = App.get_running_app().backend
        self.left_menu_width = kivy.metrics.sp(60)
        super(MainWidget, self).__init__(**kwargs)
