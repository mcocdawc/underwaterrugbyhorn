.. Underwater Rugby Horn documentation master file, created by
   sphinx-quickstart on Tue Nov 29 00:23:44 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

	 
Underwater Rugby Horn's documentation!
======================================

Currently the project consists of two programs, the main and the client. The main is intended to run an a Raspberry Pi. This pi is configured in such a way that it creates a WiFi hotspot and breadcasts the game status via UDP. The client can be run an any device running pyhton and kivy. It listens to the UDP broadcast and display the game status.

This site is genereated automatically from the self-documenting source code.

The schematic of the corresponding hardware can be found at: :download:`Underwaterhorn.pdf <../../Underwaterhorn_schematic/Underwaterhorn.pdf>`.

Complete Table of packages and modules
--------------------------------------

.. toctree::
	 :maxdepth: 0

	 main
	 client


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

