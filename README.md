# Underwater Rugby Horn

Program for the time and sound management of an underwater rugby game. It
is intended to run on a RaspberryPi. In order to test the program on other 
OSs, a dummy RPi.GPIO module is added (clicking on 'Halbzeit' simulates
pressing the main referee button). This dummy module needs to be deleted if the 
program is used on an actual RasberryPi.

The following python packages are needed:
> kivy  
> threading  
> time  
> numpy  
> RPi.GPIO (dummy module is included)  

Start the program by calling:
> python ~/underwaterrugbyhorn/main.py  

The documentation of the source code can be found at: [Source Code Docs](https://underwaterrugbyhorn.gitlab.io/underwaterrugbyhorn/)  

The schematic for the hardware can be found at: [Schematic](https://gitlab.com/UnderWaterRugbyHorn/underwaterrugbyhorn/blob/master/Underwaterhorn_schematic/Underwaterhorn.pdf)  


## Setup of the Raspberry Pi

The following settings were done on a Raspberry Pi 3 with Raspbian Jessie Light. I recommend not to start a desktop manager automatically, since kivy will just add a layer on top and the desktop manager stays responsive.  

In order to get scaling on a eGalax touch screen to work, the source file hidinput needs to me modified. Please replace the original file at *'/usr/local/lib/python2.7/dist-packages/kivy/input/providers/hidinput.py'* with the file provided in this repository. The scaling of the screen can then be done by setting the *\*\_abs\_\** values in the *'.kivy/config.txt'* as well as in *'/root/.kivy/config.ini'*.  

Something similar needs to be done for the touchring module. The file *'/usr/local/lib/python2.7/dist-packages/kivy/modules/touchring.py'* needs to be exchanged.__

You can follow this [guide](https://frillip.com/using-your-raspberry-pi-3-as-a-wifi-access-point-with-hostapd/), so that the raspberry pi starts a wireless network called *'Pi3-AP'* as soon as it boots. In order to connect to the pi within this network call *'ssh pi@172.24.1.1'*. The log-files can be viewed by either *'sudo tail -f /root/.kivy/logs/kivy\_\*.txt'* or by *'tail -f ~/.kivy/logs/kivy\_\*.txt'*.  

In order to automatically start the python script after the pi finished booting, add the line *'/home/pi/underwaterrugbyhorn/main.py &'* to the file *'/etc/rc.local'* at the end right before *'exit 0'*.  
